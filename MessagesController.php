<?php

/**
 * Messaging web service controller, allows to get messages by school and emoji string/id
 *
 */
class MessagesController
{
    /**
     * Return message by emoji or id in JSON format
     *
     * @param string $emoji input emoji string
     * @param string $school name of school
     * @param int $id message id
     *
     * @outputs json with mock message
     */
    public function getMessage($school, $emoji = null, $id = null)
    {
        $result = [
            'success' => false
        ];

        if (!is_null($emoji) || !is_null($id)) {
            try {
                $emojiModel = new Emoji();
                if (is_null($id)) {
                    $id = $emojiModel->EmojiToInt($emoji);
                } elseif (is_null($emoji)) {
                    $emoji = $emojiModel->IntToEmoji($id);
                }


                $result['success'] = true;
                $result['id'] = $id;

                // here we need to get a message from DB, but let's use its link as a mock
                $result['message'] = "Bla bla;\n\n Canonical link to this message: http://www.afterschoolapp.com/$emoji/$school";
            } catch (Exception $e) {
            }
        }

        echo json_encode($result);
    }
}