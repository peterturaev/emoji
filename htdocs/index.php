<?php

require_once('../autoload.php');

// normally we would have a Router parsing the URL and redirecting it to a corresponding controller
// but according to the task we have to rewrite URL on .htaccess level, so let's just use these GET params
if (isset($_GET['s']) && (isset($_GET['e']) || isset($_GET['id']))) {
    $school = $_GET['s'];
    $emoji = isset($_GET['e']) ? $_GET['e'] : null;
    $id = isset($_GET['id']) ? $_GET['id'] : null;

    $controller = new MessagesController();
    $controller->getMessage($school, $emoji, $id);
}