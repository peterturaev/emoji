<?php

require_once('autoload.php');
include_once('vendor/autoload.php');

class EmojiTest extends PHPUnit_Framework_TestCase
{
    private $emoji;

    public function __construct($name = null, $data = [], $dateName = '')
    {
        parent::__construct($name, $data, $dateName);
        $this->emoji = new Emoji();
    }

    public function testEmojiToInt()
    {
        $input = '😋🎈😃😏😋🎈😃💋😎';
        $expected = 55280721477;
        $int = $this->emoji->EmojiToInt($input);
        $this->assertEquals($int, $expected, "Emoji $input didn't convert to int correctly, expected $expected, got $int");
    }

    public function testIntToEmoji()
    {
        $input = 111;
        $expected = '💯😏';
        $string = $this->emoji->IntToEmoji($input);
        $this->assertEquals($string, $expected, "Int $input didn't convert to emoji correctly, expected $expected, got $string");
    }

    public function testEmojiToIntAndBack()
    {
        $input = '🙈😂😋🎈😃😈';
        $int = $this->emoji->EmojiToInt($input);
        $this->assertTrue(is_int($int));

        $string = $this->emoji->IntToEmoji($int);
        $this->assertTrue(is_string($string));
        $this->assertEquals($string, $input, "End-to-end conversion failed. Started with $input, ended with $string");
    }

    public function testIntToEmojiAndBack()
    {
        $input = '345968334';
        $string = $this->emoji->IntToEmoji($input);
        $this->assertTrue(is_string($string));

        $int = $this->emoji->EmojiToInt($string);
        $this->assertTrue(is_int($int));
        $this->assertEquals($int, $input, "End-to-end conversion failed. Started with $input, ended with $int");
    }

    public function testWrongInput()
    {
        $proceeded = false;
        try {
            $this->emoji->EmojiToInt(12345);
            $proceeded = true;
        } catch (Exception $e) {
        }

        $this->assertFalse($proceeded, "Wront input test failed. Exception was not thrown");
    }
}
