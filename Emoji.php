<?php

/**
 * Class Emoji
 *
 * This class can be used to convert emoji string to integer and back
 * 
 */
class Emoji implements EmojiInterface
{
    // the list of supported emojis
    const EMOJIS = '❤😍🙈😂💋😎💯😉😅😈😜🎉😋🎈😃😏';

    private $emojisArray;
    private $emojiIndex;

    public function __construct()
    {
        // splitting unicode string into chars; this is supposed to be 50% faster than mb_substr
        $this->emojisArray = preg_split('//u', self::EMOJIS, -1, PREG_SPLIT_NO_EMPTY);

        // building emoji index for fast lookup
        $this->emojiIndex = array_flip($this->emojisArray);
    }

    /**
     * Convert emoji string to integer
     *
     * @param String $emoji
     * @return int
     * @throws Exception
     */
    public function EmojiToInt($emoji)
    {
        // splitting unicode string into chars
        $emojiArray = preg_split('//u', $emoji, -1, PREG_SPLIT_NO_EMPTY);
        $base = count($this->emojiIndex);
        $baseStr = '';
        // looping through each smiley
        foreach ($emojiArray as $smiley) {
            if (!isset($this->emojiIndex[$smiley])) {
                throw new Exception('Invalid character');
            }

            // getting decimal representation of a smiley face - index in an EMOJIS string const
            $decSmiley = $this->emojiIndex[$smiley];
            // converting index into base = number of emojis, so that each emoji can be represented by a single digit
            $baseStr .= base_convert((string)$decSmiley, 10, $base);

        }

        // converting back to decimal for it to be an integer as required
        return (int)base_convert($baseStr, $base, 10);
    }


    /**
     * Convert integer to emoji string
     *
     * @param int $integer
     * @return string
     */
    public function IntToEmoji($integer)
    {
        // doing reverse conversions
        $base = count($this->emojiIndex);
        $baseStr = base_convert((string)$integer, 10, $base);
        $emoji = '';
        for ($i = 0; $i < strlen($baseStr); $i++) {
            $decSmiley = (int)base_convert($baseStr[$i], $base, 10);
            $emoji .= $this->emojisArray[$decSmiley];
        }

        return $emoji;
    }
}