<?php

interface EmojiInterface
{

    /**
     * @param String $emoji Emoji String to decode
     * @return Integer
     */
    public function EmojiToInt($emoji);

    /**
     * @param Integer $integer Integer to encode into Emojis
     * @return String
     */
    public function IntToEmoji($integer);
}